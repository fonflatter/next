const path = require("path");

module.exports = {
  webpack: config => {
    const directories = ["components", "lib"];
    directories.forEach(subdirectory => {
      config.resolve.alias[subdirectory] = path.resolve(__dirname, subdirectory);
    });
    return config;
  }
};
