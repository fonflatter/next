export default new Intl.DateTimeFormat("de-DE", {
  hour: "numeric",
  minute: "numeric",
  timeZone: "Europe/Berlin"
});
