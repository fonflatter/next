const apiBaseUrl = "https://test.fonflatter.de/wp-json";

export const buildApiUrl = (path, params) => {
  let url = `${apiBaseUrl}/${path}/?`;
  Object.entries(params).forEach(([key, value]) => {
    url += `${encodeURIComponent(key)}=${encodeURIComponent(value)}&`;
  });
  return url;
};
