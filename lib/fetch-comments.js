import { fetchJson } from "./fetch-json";

export const fetchComments = async commentsUrl => {
  const comments = await fetchJson(commentsUrl);
  if (comments.statusCode) {
    return comments;
  }

  return comments.map(({ id, author_name, content, date_gmt, link }) => {
    const { pathname, hash } = new URL(link);
    return {
      id,
      author: author_name,
      dateTime: `${date_gmt}Z`,
      html: content.rendered,
      url: {
        pathname: "/[year]/[month]/[day]/[slug]/",
        asPath: `${pathname}${hash}`
      }
    };
  });
};
