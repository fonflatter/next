import fetch from "isomorphic-unfetch";

export const fetchJson = async url => {
  const response = await fetch(url);
  const { status: statusCode } = response;
  if (statusCode !== 200) {
    return { statusCode };
  }

  return await response.json();
};
