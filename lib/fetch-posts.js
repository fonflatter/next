import { comicCategoryId } from "./config";
import { fetchJson } from "./fetch-json";
import { buildApiUrl } from "./build-url";

const buildPostUrl = params =>
  buildApiUrl("wp/v2/posts", {
    status: "publish",
    orderby: "date",
    order: "desc",
    ...params
  });

export const fetchPosts = async params => {
  const apiUrl = buildPostUrl(params);
  const postsJson = await fetchJson(apiUrl);
  if (postsJson.statusCode) {
    return postsJson;
  }

  const posts = postsJson.map(post => {
    const {
      id,
      title: { rendered: title },
      comic_image_url: imageUrl,
      content: { rendered: html },
      categories
    } = post;
    const dateTime = `${post.date_gmt}Z`;
    const { mouseover } = post.acf;
    const commentsUrl = `${post._links.replies[0].href}&orderby=date_gmt&order=asc`;
    const url = {
      href: "/[year]/[month]/[day]/[slug]/",
      as: new URL(post.link).pathname
    };
    return {
      id,
      categories,
      commentsUrl,
      dateTime,
      html,
      imageUrl: categories.includes(comicCategoryId) && imageUrl,
      mouseover,
      title,
      url
    };
  });

  if (params.per_page === 1) {
    const [post] = posts;
    return post || { statusCode: 404 };
  }

  return posts;
};
