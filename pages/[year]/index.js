import { fetchJson } from "lib/fetch-json";
import { buildApiUrl } from "../../lib/build-url";
import Link from "next/link";

const fetchPage = async slug => {
  const apiUrl = buildApiUrl("wp/v2/pages", {
    status: "publish",
    slug,
    per_page: 1
  });
  const pages = await fetchJson(apiUrl);
  if (pages.statusCode) {
    return pages;
  }

  const [page] = pages;
  if (!page) {
    return { statusCode: 404 };
  }

  const {
    content: { rendered: html },
    link,
    title: { rendered: title }
  } = page;
  const url = {
    href: "/[slug]/",
    as: new URL(link).pathname
  };

  return {
    html,
    title,
    url
  };
};

class StaticPage extends React.Component {
  static async getInitialProps(context) {
    const { query } = context;
    const slug = query.year; // :'-/
    return await fetchPage(slug);
  }

  render() {
    const { html, title, url } = this.props;
    return (
      <>
        <h2>
          <Link {...url}>
            <a>{title}</a>
          </Link>
        </h2>
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </>
    );
  }
}

export default StaticPage;
