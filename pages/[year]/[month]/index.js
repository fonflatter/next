import { fetchPosts } from "lib/fetch-posts";
import Post from "components/post";

class MonthPage extends React.Component {
  static async getInitialProps(context) {
    const { query } = context;
    const year = parseInt(query.year, 10);
    const month = parseInt(query.month, 10) - 1;
    const after = new Date(year, month, 1).toISOString();
    const before = new Date(year, month + 1, 1).toISOString();
    const posts = await fetchPosts({
      after,
      before,
      orderby: "date",
      order: "desc",
      per_page: 100
    });

    if (posts.statusCode) {
      return posts;
    }

    if (posts.length === 0) {
      return { statusCode: 404 };
    }

    return { posts };
  }

  render() {
    const { posts } = this.props;
    return (
      <>
        {posts.map(postProps => (
          <div key={postProps.id}>
            <Post {...postProps} />
            <div className="mb-16" />
          </div>
        ))}
      </>
    );
  }
}

export default MonthPage;
