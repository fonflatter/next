import { comicCategoryId } from "lib/config";
import { fetchComments } from "lib/fetch-comments";
import { fetchPosts } from "lib/fetch-posts";
import Post from "components/post";
import PostNavigation from "components/post-navigation";
import Comment from "components/comment";

class PostPage extends React.Component {
  static async getInitialProps(context) {
    const { query } = context;
    const { year, month, day, slug } = query;
    const parsedDate =
      year &&
      month &&
      day &&
      new Date(
        Date.UTC(parseInt(year, 10), parseInt(month, 10) - 1, parseInt(day, 10))
      );
    const dateRange = {
      before: `${(parsedDate || new Date())
        .toISOString()
        .substring(0, 10)}T01:00:00`
    };
    if (parsedDate) {
      dateRange.after = `${parsedDate.toISOString().substring(0, 10)}T00:00:00`;
    }
    console.log("dateRange:", dateRange);

    const post = await fetchPosts({
      ...dateRange,
      ...(slug ? { slug } : { categories: comicCategoryId }),
      per_page: 1
    });

    if (post.statusCode) {
      return post;
    }

    const [comments, previousPost, nextPost] = await Promise.all([
      fetchComments(post.commentsUrl),
      fetchPosts({
        before: post.dateTime,
        categories: post.categories,
        per_page: 1
      }),
      fetchPosts({
        after: post.dateTime,
        order: "asc",
        categories: post.categories,
        per_page: 1
      })
    ]);

    return {
      post,
      comments,
      previousPost,
      nextPost
    };
  }

  render() {
    const { post, previousPost, nextPost, comments } = this.props;
    return (
      <>
        <PostNavigation {...{ previousPost, nextPost }} />

        <Post {...post} />

        <div className="my-4">
          <a href={`/transcriptions${post.url.as}`} className="mx-2">
            <img
              alt="Diesen Comic transkribieren"
              title="Diesen Comic transkribieren"
              src="//www.fonflatter.de/bilder/transkript_btn.gif"
            />
          </a>
          <a href="/random" className="mx-2">
            <img
              alt="Zufallscomic"
              title="Zufallscomic"
              src="//www.fonflatter.de/bilder/zufall.gif"
            />
          </a>
        </div>

        <hr />

        <h3 id="comments">Kommentare</h3>
        <ol>
          {comments.map(props => (
            <Comment key={props.id} {...props} />
          ))}
        </ol>
      </>
    );
  }
}

export default PostPage;
