import App from "next/app";
import Error from "next/error";
import "../styles.css";
import { buildApiUrl } from "lib/build-url";
import { fetchComments } from "lib/fetch-comments";
import LeftSidebar from "components/left-sidebar";
import PageTitle from "components/page-title";
import RightSidebar from "components/right-sidebar";
import Footer from "components/footer";

const fetchSidebarData = async () => {
  const apiUrl = buildApiUrl("wp/v2/comments", {
    orderby: "date",
    order: "desc",
    per_page: 5
  });
  const lastComments = await fetchComments(apiUrl);
  return { lastComments };
};

class CustomApp extends React.Component {
  static async getInitialProps(context) {
    const { pageProps } = await App.getInitialProps(context);
    const sidebar = await fetchSidebarData();
    return {
      pageProps,
      sidebar
    };
  }

  render() {
    const { Component, pageProps, sidebar } = this.props;
    const { statusCode } = pageProps;
    return (
      <>
        <PageTitle className="w-full lg:hidden mb-4 p-2" />
        <div className="flex flex-wrap md:flex-no-wrap">
          <LeftSidebar className="hidden lg:block lg:flex-auto lg:text-right pt-20" />
          <div className="container max-w-full md:max-w-2xl text-center sm:px-2 md:pr-6 lg:px-6">
            {statusCode ? (
              <Error statusCode={statusCode} />
            ) : (
              <Component {...pageProps} />
            )}
            <Footer />
          </div>
          <RightSidebar
            className="w-full md:w-64 lg:flex-auto mt-0 md:-mt-16 lg:mt-0 ml-auto text-center md:text-left"
            {...sidebar}
          />
        </div>
      </>
    );
  }
}

export default CustomApp;
