import Link from "next/link";

class Footer extends React.Component {
  render() {
    const links = [
      { href: "/[slug]", as: "/datenschutz", text: "datenschutz" },
      { href: "/[slug]", as: "/kontakt", text: "impressum und kontakt" },
      { href: "/[slug]", as: "/archiv", text: "archiv" },
      { href: "/[slug]", as: "/ausstellung", text: "ausstellung" },
      { href: "/[slug]", as: "/geschenke", text: "geschenke" },
      { href: "/[slug]", as: "/post", text: "post" },
      { href: "/[slug]", as: "/termine", text: "termine" },
      { href: "http://shop.fonflatter.de", text: "shop" }
    ];

    return (
      <div className="text-sm border-gray-400 border-t pb-4">
        {links.map(({ href, as, text }, i) => (
          <>
            <Link key={as || href} {...{ href, as }}>
              <a>{text}</a>
            </Link>
            {i < links.length - 1 ? " | " : ""}
          </>
        ))}
      </div>
    );
  }
}

export default Footer;
