import dateFormat from "lib/date-format";
import timeFormat from "lib/time-format";

class Comment extends React.Component {
  render() {
    const { id, author, dateTime, html } = this.props;
    const parsedDateTime = new Date(dateTime);
    const commentDate = dateFormat.format(parsedDateTime);
    const commentTime = timeFormat.format(parsedDateTime);
    return (
      <li id={`comment-${id}`} className="comment">
        <strong>{author}</strong>
        {" schreibt "}
        <time dateTime={dateTime}>
          am {commentDate} um {commentTime} Uhr
        </time>
        : <div dangerouslySetInnerHTML={{ __html: html }} />
      </li>
    );
  }
}

export default Comment;
