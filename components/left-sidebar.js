import PageTitle from "./page-title";
import Link from "next/link";

const firstYear = 2005;

class LeftSidebar extends React.Component {
  render() {
    const now = new Date();
    const currentYear = now.getFullYear();

    const years = [];
    for (let i = firstYear; i <= currentYear; i++) {
      years.push(i);
    }
    years.reverse();

    const monthsForYear = year => {
      const firstMonth = year === firstYear ? 8 : 0;
      const lastMonth = year === currentYear ? now.getMonth() : 11;
      const months = [];
      for (let i = firstMonth; i <= lastMonth; i++) {
        months.push(i);
      }
      months.reverse();
      const monthFormat = new Intl.DateTimeFormat("de-DE", { month: "long" });
      return months.map(month => {
        const url = {
          pathname: "/[year]/[month]/",
          asPath: `/${year}/${month < 9 ? "0" : ""}${month + 1}/`
        };
        return {
          url,
          text: monthFormat.format(new Date(year, month))
        };
      });
    };

    return (
      <div className={`${this.props.className}`}>
        <img
          src="https://www.fonflatter.de/wp-content/themes/fonflatter/images/header.jpg"
          aria-hidden="true"
        />
        <PageTitle className="mb-4" />
        {years.map(year => (
          <details key={year} open={year === currentYear}>
            <summary>{year}</summary>
            <ul className="text-sm">
              {monthsForYear(year).map(({ text, url }) => (
                <li>
                  <Link href={url.pathname} as={url.asPath}>
                    <a>{text}</a>
                  </Link>
                </li>
              ))}
            </ul>
          </details>
        ))}
      </div>
    );
  }
}

export default LeftSidebar;
