import Link from "next/link";

class PageTitle extends React.Component {
  render() {
    return (
      <div className={`${this.props.className} px-2 lg:px-0`}>
        <h1 className="inline-block lg:block mr-4 lg:mr-0">
          <Link href="/">
            <a>fonflatter.de</a>
          </Link>
        </h1>
        <div className="inline-block">der fetzige comic</div>
      </div>
    );
  }
}

export default PageTitle;
