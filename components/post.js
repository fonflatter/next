import dateFormat from "lib/date-format";
import Link from "next/link";

class Post extends React.Component {
  render() {
    const {
      dateTime,
      title,
      imageUrl,
      mouseover,
      html,
      url
    } = this.props;
    return (
      <>
        <h2>
          <Link {...url}>
            <a>{title}</a>
          </Link>
        </h2>
        <div className="flex flex-wrap px-4 sm:px-2 md:px-0 mb-4">
          <time dateTime={dateTime} className="w-full">
            {dateFormat.format(new Date(dateTime))}
          </time>
        </div>
        {imageUrl && (
          <p>
            <img className="ml-auto mr-auto" src={imageUrl} title={mouseover} />
          </p>
        )}
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </>
    );
  }
}

export default Post;
