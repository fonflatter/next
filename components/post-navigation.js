import Link from "next/link";

const renderPostLink = (link, formatTitle, className) => {
  if (link.statusCode) return;
  const { dateTime, title, url } = link;
  return (
    <time {...{ dateTime, className }}>
      <Link {...url}>
        <a>{formatTitle(title)}</a>
      </Link>
    </time>
  );
};

class PostNavigation extends React.Component {
  render() {
    const { previousPost, nextPost } = this.props;

    return (
      <div className="flex flex-wrap px-4 sm:px-2 md:px-0 mb-4 lg:mt-4">
        {renderPostLink(previousPost, title => `« ${title}`, "mr-4 text-left")}
        {renderPostLink(nextPost, title => `${title} »`, "ml-auto text-right")}
      </div>
    );
  }
}

export default PostNavigation;
