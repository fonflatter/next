import Link from "next/link";
import dateFormat from "lib/date-format";

class RightSidebar extends React.Component {
  render() {
    const { lastComments } = this.props;
    return (
      <div className={`${this.props.className}`}>
        <a href="http://shop.fonflatter.de/" className="block">
          <img src="https://www.fonflatter.de/bilder/zehn_08s.png" alt="shop" />
        </a>
        <a href="http://shop.fonflatter.de/" className="block">
          <img
            src="https://www.fonflatter.de/bilder/menu_shop.png"
            alt="Fredshop"
          />
        </a>
        <a href="https://www.fonflatter.de/geschenke" className="block">
          <img
            src="https://www.fonflatter.de/bilder/menu_geschenke.png"
            alt="Geschenke"
          />
        </a>

        <form
          className="ml-auto mr-auto md:m-0 w-48"
          action="https://www.fonflatter.de/archiv/"
        >
          <h3>suche</h3>
          <input type="hidden" name="_year" value={new Date().getFullYear()} />
          <input
            type="text"
            name="q"
            className="block mb-2 w-full text-sm px-2"
          />
          <button type="submit" className="block ml-auto">
            los
          </button>
        </form>

        {!lastComments.statusCode && (
          <>
            <h3>echos</h3>
            <ul className="text-sm">
              {lastComments.map(({ id, author, dateTime, url }) => (
                <li key={id}>
                  <Link href={url.pathname} as={url.asPath}>
                    <a>
                      {author} am {dateFormat.format(new Date(dateTime))}
                    </a>
                  </Link>
                </li>
              ))}
            </ul>
          </>
        )}

        <h3>Verknüpfungen</h3>
        <ul className="text-sm">
          <li>
            <a href="https://www.facebook.com/fonflatter">facebook</a>
          </li>
          <li>
            <a href="http://fred-o-mat.spreadshirt.net/">Fredshop</a>
          </li>
          <li>
            <a href="//fredthebat.com/">fredthebat</a>
          </li>
          <li>
            <a href="https://www.instagram.com/bastianmelnyk/">instagram</a>
          </li>
          <li>
            <a href="http://krakeleien.spreadshirt.de/">Krakeleien-Shop</a>
          </li>
          <li>
            <a href="https://de.pinterest.com/linejuice/">pinterest</a>
          </li>
          <li>
            <a href="https://www.redbubble.com/de/people/fredthebat/shop">
              Redbubble-Shop
            </a>
          </li>
          <li>
            <a href="http://fredthebat.tumblr.com/">tumblr</a>
          </li>
          <li>
            <a href="https://twitter.com/bastianmelnyk">twitter</a>
          </li>
          <li>
            <a href="//morast.eu">‌‌morast</a>{" "}
          </li>
        </ul>
      </div>
    );
  }
}

export default RightSidebar;
